#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <float.h>
#include <math.h>

#include "qdbmp/qdbmp.h"

typedef struct vec3{
    double x;
    double y;
    double z;
}vec3_t;

struct atom{

    uint16_t atomtype; 
    uint32_t col;   //atom color
    struct vec3 pos; //atom center position
    double rad; //atom radius
    uint8_t isDrawn;  //has already been drawn
    size_t nNeighbours; //number of neighbours atoms
    struct atom** neighbours;    //pointer to neighbour atoms (atoms at distances less than connection distance)
    size_t* neighidx;    //neighbour index
};

struct plane{

    struct vec3 p0;     //plane point
    struct vec3 n;      //plane normal

};

struct cam{

    struct vec3 pos;    //camera position
    struct vec3 dir;    //camera direction
};


struct ray{

    struct vec3 orig;
    struct vec3 dir;

};

vec3_t VEC3_ADD(vec3_t a, vec3_t b){

    return (vec3_t){a.x+b.x,a.y+b.y,a.z+b.z};

}

vec3_t VEC3_SUB(vec3_t a, vec3_t b){

    return (vec3_t){a.x-b.x,a.y-b.y,a.z-b.z};

}

double VEC3_DOT(vec3_t a, vec3_t b){

    return a.x*b.x+a.y*b.y+a.z*b.z;

}

vec3_t VEC3_NORMALIZE(vec3_t v){
    
    double mag = v.x*v.x + v.y*v.y + v.z*v.z;
    mag = sqrt(mag);

    return (vec3_t){v.x/mag, v.y/mag, v.z/mag};


}
double VEC3_MAGNITUDE(vec3_t v){
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

vec3_t VEC3_MULTIPLY(vec3_t v, double f){
    return (vec3_t){v.x*f,v.y*f,v.z*f};
}

vec3_t VEC3_CROSS(vec3_t a, vec3_t b){
    
    vec3_t ret;

    ret.x = a.y*b.z - a.z*b.y;
    ret.y = a.x*b.z - a.z*b.x;
    ret.z = a.x*b.y - a.y*b.z;

    return ret;

}

double Clamp(double x, double min, double max){

    if(x<min){
        return min;
    }
    else if(x>max){
        return max;
    }
    else{
        return x;
    }

}

uint8_t SPH_INTERSECT(struct ray* ray, struct atom* sphere, double* zdepth, double* dotproduct){

    float t0, t1;
    float radsqr = sphere->rad*sphere->rad;

    vec3_t L = VEC3_SUB(sphere->pos,ray->orig);
    float tca = VEC3_DOT(L,ray->dir);
    float d2 = VEC3_DOT(L,L) - tca*tca;
    if(d2>radsqr) return 0; //no intersection
    float thc = sqrt(radsqr - d2);
    t0 = tca - thc;
    t1 = tca + thc;

    if(t0>t1){
        float temp = t0;
        t0 = t1;
        t1 = temp;
    }

    if(t0 < 0){
        t0 = t1;
        if(t0 < 0)
            return 0;
    }

    *zdepth = t0;
    
    vec3_t ipoint = VEC3_ADD(ray->orig,VEC3_MULTIPLY(ray->dir,t0));
    
    vec3_t norm = VEC3_SUB(ipoint,sphere->pos);

    vec3_t viewdir = (vec3_t){0.0,0.0,-1.0f};

    //*dotproduct = Clamp(fabs(VEC3_DOT(viewdir,norm)),0.0,1.0);
    *dotproduct = pow(t1-t0,2);
    return 1;
}

vec3_t VEC3_AVERAGE(vec3_t* vecs, uint32_t nvecs){
    uint32_t i = 0;
    vec3_t avgvec = (vec3_t){0.0,0.0,0.0};
    
    for(i=0;i<nvecs;i++){
        avgvec.x += vecs[i].x/nvecs;
        avgvec.y += vecs[i].y/nvecs;
        avgvec.z += vecs[i].z/nvecs;
    }

    return avgvec;
}

vec3_t VEC3_ROTATE_EULER(vec3_t v, char axis, double angle){

    vec3_t r;

    switch(axis){

        case 'x':

            r.x = v.x;
            r.y = cos(angle) * v.y - sin(angle) * v.z;
            r.z = sin(angle) * v.y + cos(angle) * v.z;

            break;


        case 'y':

            r.y = v.y;
            r.x = cos(angle) * v.x + sin(angle) * v.z;
            r.z = -sin(angle) * v.x + cos(angle) * v.z;

            break;


        case 'z':

            r.z = v.z;
            r.x = cos(angle) * v.x - sin(angle) * v.y;
            r.z = sin(angle) * v.x + cos(angle) * v.y;

            break;

        default:
            
            r = v;

            break;

    }

    return r;

}

struct line{
    vec3_t v[2];    //vertices
    uint32_t col;   //color
    double thick;   //thickness
};

double LineRayDistance(struct ray r, struct line l, double* zdepth){

    double dist = 0.0;

    vec3_t l_dir = VEC3_SUB(l.v[1],l.v[0]);   //line direction vector, non normalized
    double l_dir_mag = VEC3_MAGNITUDE(l_dir);
    l_dir = VEC3_MULTIPLY(l_dir,1.0/l_dir_mag); //normalize dir vector

    vec3_t n = VEC3_CROSS(r.dir,l_dir);

    double dotprodv0 = VEC3_DOT(n,VEC3_SUB(l.v[0],r.orig));
    double dotprodv1 = VEC3_DOT(n,VEC3_SUB(l.v[1],r.orig));

    if(fabs(dotprodv0) > l_dir_mag){
        dist = fabs(dotprodv0);
        
    }
    else if(fabs(dotprodv1) > l_dir_mag){
        dist = fabs(dotprodv1);
    }
    else{
        dist = fabs(dotprodv0)/VEC3_MAGNITUDE(n);
        
    }

    vec3_t zvec = VEC3_SUB(l.v[0],r.orig);
    *zdepth = l.v[0].z + l_dir.z * VEC3_DOT(zvec,l_dir);
    
    //printf("%lf %lf\n",dist,*zdepth);
    return dist;
}

vec3_t VEC3_PROJECT(vec3_t origin, vec3_t normal, vec3_t point){
    
    vec3_t vOO = VEC3_SUB(point,origin);   //line point to plane origin, distance vector

    double pointdist = VEC3_DOT(vOO,normal); //actual distance of previously calculated vector, along plane normal

    vec3_t pp = VEC3_SUB(point,VEC3_MULTIPLY(normal,pointdist));    //get point coordinates

    return pp;
}

uint8_t LineRayIntersect(struct ray* r, struct line* l, double* zdepth, double* dist2line){
    
    
    vec3_t l_dir = VEC3_SUB(l->v[0],l->v[1]);
    double l_dir_mag = VEC3_MAGNITUDE(l_dir);
    if(l_dir_mag != 0.0)
        l_dir = VEC3_MULTIPLY(l_dir,1.0/l_dir_mag); //get normalized line direction vector
    else{
        return 0;   //line is 0 length, no intersection
    }

    vec3_t distvec = VEC3_CROSS(r->dir,l_dir);   //get vector perpendicular to both lines

    

    vec3_t origvec0 = VEC3_SUB(l->v[0],r->orig); 
    vec3_t origvec1 = VEC3_SUB(l->v[1],r->orig);
    
    double dist = VEC3_DOT(distvec,origvec0);

    if(fabs(dist) < l->thick){

        if(fabs(VEC3_DOT(origvec1,l_dir) - l_dir_mag) < l->thick ){  //check if line-origin vectors are opposite direction (intersect point is with the line's length)
            if(zdepth != NULL){
                *zdepth = 0.0;  //standin
            }
            return 1;
        }
        else
            return 0;
        
    }

    return 0;
}

typedef struct CIF{

    uint32_t natms;
    struct atom *atms;
    uint32_t nconns;    //number of connections between atoms
    struct line *conn;
    vec3_t cell_vecs[3];    //xyz = abc
    vec3_t cell_vtx[8];     //unit cell vertices 

} CIF_t;

void CreateUnitCell(CIF_t *ciff){

    size_t i = 0;

    ciff->cell_vtx[0].x = 0.0; 
    ciff->cell_vtx[0].y = 0.0; 
    ciff->cell_vtx[0].z = 0.0;
    
    ciff->cell_vtx[1].x = ciff->cell_vtx[0].x + ciff->cell_vecs[0].x;
    ciff->cell_vtx[1].y = ciff->cell_vtx[0].y + ciff->cell_vecs[0].y;
    ciff->cell_vtx[1].z = ciff->cell_vtx[0].z + ciff->cell_vecs[0].z;
    
    ciff->cell_vtx[2].x = ciff->cell_vtx[0].x + ciff->cell_vecs[1].x;
    ciff->cell_vtx[2].y = ciff->cell_vtx[0].y + ciff->cell_vecs[1].y;
    ciff->cell_vtx[2].z = ciff->cell_vtx[0].z + ciff->cell_vecs[1].z;

    ciff->cell_vtx[3].x = ciff->cell_vtx[1].x + ciff->cell_vecs[1].x;
    ciff->cell_vtx[3].y = ciff->cell_vtx[1].y + ciff->cell_vecs[1].y;
    ciff->cell_vtx[3].z = ciff->cell_vtx[1].z + ciff->cell_vecs[1].z;

    ciff->cell_vtx[4].x = ciff->cell_vtx[0].x + ciff->cell_vecs[2].x;
    ciff->cell_vtx[4].y = ciff->cell_vtx[0].y + ciff->cell_vecs[2].y;
    ciff->cell_vtx[4].z = ciff->cell_vtx[0].z + ciff->cell_vecs[2].z;

    ciff->cell_vtx[5].x = ciff->cell_vtx[1].x + ciff->cell_vecs[2].x;
    ciff->cell_vtx[5].y = ciff->cell_vtx[1].y + ciff->cell_vecs[2].y;
    ciff->cell_vtx[5].z = ciff->cell_vtx[1].z + ciff->cell_vecs[2].z;

    ciff->cell_vtx[6].x = ciff->cell_vtx[2].x + ciff->cell_vecs[2].x;
    ciff->cell_vtx[6].y = ciff->cell_vtx[2].y + ciff->cell_vecs[2].y;
    ciff->cell_vtx[6].z = ciff->cell_vtx[2].z + ciff->cell_vecs[2].z;

    ciff->cell_vtx[7].x = ciff->cell_vtx[3].x + ciff->cell_vecs[2].x;
    ciff->cell_vtx[7].y = ciff->cell_vtx[3].y + ciff->cell_vecs[2].y;
    ciff->cell_vtx[7].z = ciff->cell_vtx[3].z + ciff->cell_vecs[2].z;


    uint32_t tris[12][3];
    //face 1
    tris[0][0] = 0; tris[0][1] = 1; tris[0][2] = 2;
    tris[1][0] = 1; tris[1][1] = 2; tris[1][2] = 3;
    //face 2
    tris[2][0] = 4; tris[2][1] = 5; tris[2][2] = 6;
    tris[3][0] = 5; tris[3][1] = 6; tris[3][2] = 7;
    //face 3
    tris[4][0] = 0; tris[4][1] = 1; tris[4][2] = 4;
    tris[5][0] = 1; tris[5][1] = 4; tris[5][2] = 5;
    //face 4
    tris[6][0] = 2; tris[6][1] = 3; tris[6][2] = 6;
    tris[7][0] = 3; tris[7][1] = 6; tris[7][2] = 7;
    //face 5
    tris[8][0] = 0; tris[8][1] = 2; tris[8][2] = 4;
    tris[9][0] = 2; tris[9][1] = 6; tris[9][2] = 4;
    //face 6
    tris[10][0] = 1; tris[10][1] = 3; tris[10][2] = 5;
    tris[11][0] = 3; tris[11][1] = 7; tris[11][2] = 5;

    for(i=0;i<8;i++)
        printf("%f %f %f\n",ciff->cell_vtx[i].x,ciff->cell_vtx[i].y,ciff->cell_vtx[i].z);

    FILE* fp = NULL;

    fp = fopen("unitcell.obj","w+");

    if(fp == NULL){
        printf("ERROR: Failed to create obj file for unit cell mesh.\n");
        return;
    }

    for(i=0;i<8;i++){
        fprintf(fp,"v %f %f %f\n",ciff->cell_vtx[i].x,ciff->cell_vtx[i].y,ciff->cell_vtx[i].z);
    }
    
    fprintf(fp,"o UnitCell\n");

    for(i=0;i<12;i++){
        fprintf(fp,"f %d %d %d\n",tris[i][0]+1,tris[i][1]+1,tris[i][2]+1);
    }

    fclose(fp);
    
}

int LoadPOSCARCIF(char* filename, CIF_t* ciff){

    FILE* fp = NULL;

    fp = fopen(filename, "r");

    if(fp == NULL){
        printf("Error opening file: Corrupt or Missing!\n");
        return -1;
    }
    #define LINEBUF_LEN 512
    char linebuf[LINEBUF_LEN];
    float param0;
    uint32_t i = 0;
    vec3_t a[3];
    uint16_t atomtype = 0;
    uint32_t natoms = 0;

    fgets(linebuf,LINEBUF_LEN,fp);

    if(fgets(linebuf,LINEBUF_LEN,fp) != NULL){
        sscanf(linebuf,"%lf",&param0);
        //printf("%lf\n",param0);
    }

    for(i=0;i<3;i++){
        if(fgets(linebuf,LINEBUF_LEN,fp) != NULL){
            sscanf(linebuf,"%lf %lf %lf",&(a[i].x),&(a[i].y),&(a[i].z));
            //printf("%lf,%lf,%lf\n",a[i].x,a[i].y,a[i].z);
        }
    }

    memcpy(ciff->cell_vecs,a,3*sizeof(vec3_t));

    if(fgets(linebuf,LINEBUF_LEN,fp) != NULL){
        //sscanf(linebuf,"%s",&atomtype);
        //printf("%s\n",atomtype);
    }

    if(fgets(linebuf,LINEBUF_LEN,fp) != NULL){
        sscanf(linebuf,"%ld",&(ciff->natms));
        //printf("%ld\n",ciff->natms);
    }

    fgets(linebuf,LINEBUF_LEN,fp);

    ciff->atms = malloc(ciff->natms * sizeof(struct atom));
    if(ciff->atms == NULL){
        printf("OOPS!\n");
        return 1;
    }
    for(i=0;i<ciff->natms;i++){
        if(fgets(linebuf,LINEBUF_LEN,fp) != NULL){
            sscanf(linebuf,"%lf %lf %lf",&(a[0].x),&(a[0].y),&(a[0].z));
            //printf("%lf,%lf,%lf\n",a[0].x,a[0].y,a[0].z);
            ciff->atms[i].pos = a[0];
            ciff->atms[i].rad = 0.25f;
            ciff->atms[i].atomtype = atomtype;
        }
    }
    
    fclose(fp);


    return 0;
}

#define LigandDistance 1.9

int main(int argc, char** argv){

    uint32_t WIN_RESX = 1024;
    uint32_t WIN_RESY = 768;
    size_t i, j, k, n;
    size_t npixels = WIN_RESX * WIN_RESY;

    uint32_t *framebuf = malloc(npixels*sizeof(uint32_t));
    double *zbuf = malloc(npixels*sizeof(double));

    memset(framebuf,0x00,npixels*sizeof(uint32_t));
    
    for(i=0;i<npixels;i++){
        zbuf[i] = DBL_MIN;
    }

    BMP* bmp_output = BMP_Create(WIN_RESX,WIN_RESY,32);

    CIF_t cif_struct;

    LoadPOSCARCIF("./cif/POSCAR_6p6_hexalinahdo_c.cif",&cif_struct);

    CreateUnitCell(&cif_struct);

    printf("Calculating neighbours....\n");

    vec3_t atom_dist;
    double atom_dist_mag = 0.0f;
    uint8_t atomInList = 0;
    for(i=0;i<cif_struct.natms;i++){
        //cycle through all atoms to find their neighbours
        for(j=0;j<cif_struct.natms;j++){
            //cycle through all atoms again
            if(i!=j){ //ignore same atom

                //calculate atom-atom distance

                atom_dist = VEC3_SUB(cif_struct.atms[i].pos,cif_struct.atms[j].pos);

                atom_dist_mag = VEC3_MAGNITUDE(atom_dist);

                if(atom_dist_mag<=LigandDistance){
                    //add j to i
                    if(cif_struct.atms[i].neighbours == NULL){
                        cif_struct.atms[i].neighbours = malloc(1*sizeof(struct atom*)); //allocate one atom pointer
                        cif_struct.atms[i].neighidx = malloc(1*sizeof(size_t)); //allocate one atom pointer
                        if(cif_struct.atms[i].neighbours == NULL || cif_struct.atms[i].neighidx == NULL){
                            cif_struct.atms[i].nNeighbours = 0;
                        }
                        else{
                            cif_struct.atms[i].nNeighbours=1;
                            cif_struct.atms[i].neighbours[cif_struct.atms[i].nNeighbours-1] = &(cif_struct.atms[j]);  //add pointer to neighbour atom
                            cif_struct.atms[i].neighidx[cif_struct.atms[i].nNeighbours-1] = j;  //add pointer to neighbour atom
                        }
                    }
                    else{
                        atomInList = 0;
                        for(k=0;k<cif_struct.atms[i].nNeighbours;k++){
                            if(cif_struct.atms[i].neighbours[k] == &(cif_struct.atms[j])){
                                atomInList = 1;
                            }
                        }
                        if(!atomInList){
                            cif_struct.atms[i].neighbours = realloc(cif_struct.atms[i].neighbours,(cif_struct.atms[i].nNeighbours+1) * sizeof(struct atom*)); //allocate more atom pointers
                            cif_struct.atms[i].neighidx = realloc(cif_struct.atms[i].neighidx,(cif_struct.atms[i].nNeighbours+1) * sizeof(size_t)); //allocate more atom pointers
                            if(cif_struct.atms[i].neighbours == NULL || cif_struct.atms[i].neighidx == NULL){
                              
                            }
                            else{
                                cif_struct.atms[i].nNeighbours++;
                                cif_struct.atms[i].neighbours[cif_struct.atms[i].nNeighbours-1] = &(cif_struct.atms[j]);  //add pointer to neighbour atom   
                                cif_struct.atms[i].neighidx[cif_struct.atms[i].nNeighbours-1] = j;  //add pointer to neighbour atom    
                            }
                        }
                    }
                    //add i to j
                    if(cif_struct.atms[j].neighbours == NULL ){
                        cif_struct.atms[j].neighbours = malloc(1*sizeof(struct atom*)); //allocate one atom pointer
                        cif_struct.atms[j].neighidx = malloc(1*sizeof(size_t)); //allocate one atom pointer
                        if(cif_struct.atms[j].neighbours == NULL || cif_struct.atms[j].neighidx == NULL){
                            cif_struct.atms[j].nNeighbours = 0;
                            
                        }
                        else{
                            cif_struct.atms[j].nNeighbours=1;
                            cif_struct.atms[j].neighbours[cif_struct.atms[j].nNeighbours-1] = &(cif_struct.atms[i]);  //add pointer to neighbour atom
                            cif_struct.atms[j].neighidx[cif_struct.atms[j].nNeighbours-1] = i;  //add pointer to neighbour atom
                        }
                    }
                    else{
                        atomInList = 0;
                        for(k=0;k<cif_struct.atms[j].nNeighbours;k++){
                            if(cif_struct.atms[j].neighbours[k] == &(cif_struct.atms[i])){
                                atomInList = 1;
                               
                            }
                        }
                        if(!atomInList){
                            cif_struct.atms[j].neighbours = realloc(cif_struct.atms[j].neighbours,(cif_struct.atms[j].nNeighbours+1) * sizeof(struct atom*)); //allocate more atom pointers
                            cif_struct.atms[j].neighidx = realloc(cif_struct.atms[j].neighidx,(cif_struct.atms[j].nNeighbours+1) * sizeof(size_t)); //allocate more atom pointers
                            if(cif_struct.atms[j].neighbours == NULL || cif_struct.atms[j].neighidx == NULL){
                                
                            }
                            else{
                                cif_struct.atms[j].nNeighbours++;
                                cif_struct.atms[j].neighidx[cif_struct.atms[j].nNeighbours-1] = i;
                                cif_struct.atms[j].neighbours[cif_struct.atms[j].nNeighbours-1] = &(cif_struct.atms[i]);
                            }
                        }
                    }
                }
            }
        }
    }

    FILE* fp = fopen("cneighbour.txt","w+");

    if(fp == NULL)
        printf("Cannot create output file\n");

    size_t maxnneigh = 0;

    for(i=0;i<cif_struct.natms;i++){
        if(cif_struct.atms[i].nNeighbours > maxnneigh)
            maxnneigh = cif_struct.atms[i].nNeighbours;
    }

    size_t* neighbin = calloc((maxnneigh+1),sizeof(size_t));

    for(i=0;i<cif_struct.natms;i++){
        fprintf(fp,"%ld\t%lf\t%lf\t%lf\t",i,cif_struct.atms[i].pos.x,cif_struct.atms[i].pos.y,cif_struct.atms[i].pos.z);
        neighbin[cif_struct.atms[i].nNeighbours] += 1;
        for(j=0;j<cif_struct.atms[i].nNeighbours;j++){
            fprintf(fp,"%ld\t",cif_struct.atms[i].neighidx[j]);
        }
        fprintf(fp,"\n");
    }

    fclose(fp);
    printf("Neighbour hist:\t");
    for(i=0;i<=maxnneigh;i++){
        printf("%ld\t",neighbin[i]);
    }
    printf("\n");

    printf("Rendering...\n");
    uint32_t render_nBounces = 1;

    struct ray ray;

    double zval = DBL_MIN;
    double shade = 0.0;
    vec3_t meanvec = {0.0};

    for(n=0;n<cif_struct.natms;n++){
        cif_struct.atms[n].pos = VEC3_ROTATE_EULER(cif_struct.atms[n].pos,'x',M_PI);
        cif_struct.atms[n].col = 0x00FFFFFF;
        
        meanvec.x += cif_struct.atms[n].pos.x/cif_struct.natms;
        meanvec.y += cif_struct.atms[n].pos.y/cif_struct.natms;
        meanvec.z += cif_struct.atms[n].pos.z/cif_struct.natms;
    }

    struct line ltest;

    ltest.v[0] = cif_struct.atms[0].pos;
    ltest.v[1] = cif_struct.atms[5].pos;
    ltest.col = 0x0000FFFF;
    ltest.thick = 0.1;

    uint8_t R,G,B;
    uint32_t a = 0;

    size_t maxnNeigh = 0;

    for(j=0;j<WIN_RESY;j++){
        for(i=0;i<WIN_RESX;i++){
            k = i + j*WIN_RESX;
            //ray.orig = VEC3_AVERAGE(cif_struct.cell_vtx, 8);
            ray.orig = meanvec;
            //ray.dir = (vec3_t){0.5-1.0*i/WIN_RESX,(0.5-1.0*j/WIN_RESY)*(1.0*WIN_RESY/WIN_RESX),-1.0};
            ray.orig.x += 30*(0.5-1.0*i/WIN_RESX);
            ray.orig.y += 30*(0.5-1.0*j/WIN_RESY)*(1.0*WIN_RESY/WIN_RESX);
            ray.orig.z += 35.0;
            ray.dir = (vec3_t){0.0,0.0,-1.0};
            ray.dir = VEC3_NORMALIZE(ray.dir);

            //calculate ray intersection with sphere
            
            for(n=0;n<cif_struct.natms;n++){
                
                if(SPH_INTERSECT(&ray,&cif_struct.atms[n],&zval,&shade)){
                    
                    if(zbuf[k]<zval){

                        zbuf[k] = DBL_MIN;

                        //uint8_t R =  cif_struct.atms[n].col & 0xFF;
                        //uint8_t G = (cif_struct.atms[n].col>>8) & 0xFF;
                        //uint8_t B = (cif_struct.atms[n].col>>16) & 0xFF;

                        //framebuf[k] = (uint32_t)(R*shade) | ((uint32_t)(G*shade)<<8) | ((uint32_t)(B*shade)<<16);
                        if(cif_struct.atms[n].nNeighbours > maxnNeigh){
                            maxnNeigh = cif_struct.atms[n].nNeighbours;
                        }
                        switch(cif_struct.atms[n].nNeighbours){

                            case 0:
                                R = G = B = 255;
                                break;
                            case 1:
                                R = 255;
                                G = B = 0;
                                break;
                            case 2:
                                G = 255;
                                R = B = 0;
                                break;
                            case 3:
                                B = 255;
                                R = G = 0;
                                break;
                            case 4:
                                R = 255;
                                G = 255;
                                B = 0;
                                break;
                            case 5:
                                R = 0;
                                G = 255;
                                B = 255;
                                break;
                            case 6:
                                R = 255;
                                G = 0;
                                B = 255;
                                break;
                        }

                        framebuf[k] = (uint32_t)(B*shade) | ((uint32_t)(G*shade)<<8) | ((uint32_t)(R*shade)<<16);

                    }
                }
            }
            

            /*if(LineRayIntersect(&ray,&ltest,&zval,NULL)){
                        uint8_t R = ltest.col & 0xFF;
                        uint8_t G = (ltest.col>>8) & 0xFF;
                        uint8_t B = (ltest.col>>16) & 0xFF;

                        framebuf[k] = (uint32_t)(R) | ((uint32_t)(G)<<8) | ((uint32_t)(B)<<16);    
            }*/
            

            /*double hitdist = LineRayDistance(ray,ltest,&zval);
           
            if(hitdist<ltest.thick){
                    if(zbuf[k]<zval){
                        zbuf[k] = zval;
                        //framebuf[k] = ceil(cif_struct.atms[n].col*shade);
                        uint8_t R = ltest.col & 0xFF;
                        uint8_t G = (ltest.col>>8) & 0xFF;
                        uint8_t B = (ltest.col>>16) & 0xFF;

                        framebuf[k] = (uint32_t)(R) | ((uint32_t)(G)<<8) | ((uint32_t)(B)<<16);
                    }                
            }*/
        }
    }
    
    printf("%ld\n",maxnNeigh);

    for(j=0;j<WIN_RESY;j++){
        for(i=0;i<WIN_RESX;i++){
            k = i + j * WIN_RESX;
            BMP_SetPixelRGB(bmp_output,i,j,framebuf[k]>>16,framebuf[k]>>8,framebuf[k]);
        }
    }

    BMP_WriteFile(bmp_output,"out.bmp");
    printf("Cleaning...\n");

    BMP_Free(bmp_output);

    free(framebuf);
    free(zbuf);

    for(i=0;i<cif_struct.natms;i++){
        free(cif_struct.atms[i].neighbours);
        free(cif_struct.atms[i].neighidx);
    }
    free(cif_struct.atms);
    
    printf("Done...\n");
    return EXIT_SUCCESS;
}